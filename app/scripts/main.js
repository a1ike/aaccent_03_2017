$(document).ready(function() {

  //$('.sub-header').hide();
  $('.sub-header').addClass('sub-header_dark');
  $('.sub-header__title').html('Весь интернет-маркетинг в одной услуге');

  if ($(window).width() >= 1024) {
    // do some magic

    $(window).on('scroll', function() {

      var value = $(this).scrollTop();

      if (value > $('.main-how__first').offset().top + 500) {
        $('.main-perfomance').hide();
      } else {
        $('.main-perfomance').show();
      };

      if (value > $('.main-how__first').offset().top - 144) {

        $('.main-header').hide(200);
        $('.sub-header').show(200);
        $('.sub-header').addClass('sub-header_fixed');
        $('.sub-header').addClass('sub-header_dark');
        $('.sub-header').removeClass('sub-header_light');
        $('.sub-header__title').html('Весь интернет-маркетинг в одной услуге');
      } else {
        $('.main-header').show(200);
        $('.sub-header').removeClass('sub-header_fixed');
        //$('.sub-header').hide(200);
      };

      if (value > $('.main-how__mounth').offset().top - 144) {
        $('.sub-header').addClass('sub-header_dark');
        $('.sub-header').removeClass('sub-header_light');
        $('.sub-header__title').html('Как работает performance-маркетинг');
      };

      if (value > $('.main-marketing__cards').offset().top - 144) {
        $('.sub-header').addClass('sub-header_light');
        $('.sub-header').removeClass('sub-header_dark');
        $('.sub-header__title').html('Весь интернет-маркетинг в одной услуге');
      };

      if (value > $('.main-team').offset().top - 144) {
        $('.sub-header').addClass('sub-header_dark');
        $('.sub-header').removeClass('sub-header_light');
        $('.sub-header__title').html('Работайте с агентством «а’Акцент»');
      };

      if (value > $('.main-customers').offset().top - 144) {
        $('.sub-header').addClass('sub-header_light');
        $('.sub-header').removeClass('sub-header_dark');
        $('.sub-header__title').html('Клиенты и кейсы');
      };

      if (value > $('.main-begin').offset().top - 144) {
        $('.sub-header').addClass('sub-header_dark');
        $('.sub-header').removeClass('sub-header_light');
        $('.sub-header__title').html('С чего начать и сколько стоит');
      };

      // Инструменты при прокрутке

      if (value > $('.main-how__mounth:eq(0)').offset().top - 500) {
        $('.main-how__card:nth-child(9)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(10)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(14)').removeClass('main-how__card_active');
      };

      if (value > $('.main-how__mounth:eq(0)').offset().top - 200) {
        $('.main-how__card:nth-child(9)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(10)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(14)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(1)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(3)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(20)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(19)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(17)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(11)').removeClass('main-how__card_active');
      };

      if (value > $('.main-how__mounth:eq(1)').offset().top - 200) {
        $('.main-how__card:nth-child(1)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(3)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(20)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(19)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(17)').removeClass('main-how__card_active');
        $('.main-how__card:nth-child(11)').removeClass('main-how__card_active');
      };

      if (value > $('.main-how__mounth:eq(2)').offset().top - 200) {
        $('.main-how__card:nth-child(19)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(17)').addClass('main-how__card_active');
        $('.main-how__card:nth-child(11)').addClass('main-how__card_active');
      };

    });

    $(function() { // wait for document ready
      // init
      var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
          triggerHook: 'onLeave'
        }
      });

      var controllerDuration = $('.one').innerHeight();

      $('.main-perfomance').each(function() {

        new ScrollMagic.Scene({
            triggerElement: this
          })
          .setPin(this)
          .addTo(controller);

      });

      $('.one, .three').each(function() {

        new ScrollMagic.Scene({
            duration: 0,
            triggerElement: this,
            offset: 1500
          })
          .setPin(this)
          .addTo(controller);

      });

      // Scroll Effects / Coming

      var comingController = new ScrollMagic.Controller({
        globalSceneOptions: {
          triggerHook: 'onLeave',
        }
      });

      var comingDuration = $('.main-how').innerHeight() - $('.main-how__companies').innerHeight() / 1;

      var comingScene = new ScrollMagic.Scene({ triggerElement: '.main-how', duration: comingDuration, offset: 0 })
        .setPin('.main-how__companies')
        .addTo(comingController);

      // Scroll Effects / Team

      var teamController = new ScrollMagic.Controller({
        globalSceneOptions: {
          triggerHook: 'onLeave',
        }
      });

      var teamDuration = $('.main-team').innerHeight() - $('.main-team__about').innerHeight() / 1;

      var teamScene = new ScrollMagic.Scene({ triggerElement: '.main-team', duration: teamDuration, offset: 0 })
        .setPin('.main-team__about')
        .addTo(teamController);

    });

  }

  var integerDivision = function(a, b) {
    return (a - a % b) / b;
  }

  /*
   * РўСЂРµСѓРіРѕР»СЊРЅРёРєРё.
   */
  var Triangles = {
    CELL_WIDTH: 36.5,
    CELL_HEIGHT: 36.5,
    triangles: {},
    lastTriangle: '',
    previousCoords: null,
    busy: false,
    colors: [
      '#BE1E2D',
      '#ED1C24',
      '#F15A29',
      '#F7941E',
      '#FBB040',
      '#FFF200',
      '#D7DF23',
      '#8DC63F',
      '#00A651',
      '#006838',
      '#00A79D',
      '#27AAE1',
      '#1C75BC',
      '#2B3990',
      '#262262',
      '#662D91',
      '#92278F',
      '#9E1F63',
      '#DA1C5C',
      '#EE2A7B'

      /*'#c63a3a',
      '#c6623a',
      '#c6903a',
      '#c6be3a',
      '#a0c63a',
      '#6fc63a',
      '#41c63a',
      '#3ac665',
      '#3ac690',
      '#3ac693',
      '#3ac6c1',
      '#3a9dc6',
      '#3a6fc6',
      '#3a41c6',
      '#653ac6',
      '#903ac6',
      '#c43ac6',
      '#c63a9d',
      '#c63a6f'*/
    ],
    currentColor: 0,
    init: function() {
      var self = this;
      this.container = $('#triangles');
      if (1) {
        this.halfCellWidth = integerDivision(this.CELL_WIDTH, 2);
        this.halfCellHeight = integerDivision(this.CELL_HEIGHT, 2);
        this.paper = Raphael(this.container.get(0));
        $(window).resize(function() {
          self.paper.setSize(self.container.width(), self.container.height());
        }).trigger('resize');
        var mousemove = function(e) {
            var pos = self.container.offset();
            self.mousemove({
              x: e.pageX - pos.left,
              y: e.pageY - pos.top
            });
          }
          //this.container.mousemove (mousemove);
        this.container.parent().find('*').mousemove(mousemove);
        //this.container.mouseout (function () {
        //  console.log ('a');
        //})
      }
    },
    mousemove: function(coords, mode2) {
      if (!mode2 && this.busy)
        return;
      if (!mode2)
        this.busy = true;
      if (!mode2 && this.previousCoords !== null) {
        var ax = Math.abs(this.previousCoords.x - coords.x);
        var ay = Math.abs(this.previousCoords.y - coords.y)
        if (ax > 1 || ax > 1) {
          var i, k;
          var directionX = this.previousCoords.x < coords.x ? 1 : -1;
          var directionY = this.previousCoords.y < coords.y ? 1 : -1;
          if (ax > ay) {
            for (k = 0, i = this.previousCoords.x; directionX == 1 ? i <= coords.x : i >= coords.x; k++, i += directionX) {
              this.mousemove({
                x: i,
                y: this.previousCoords.y + Math.floor((ay * k) / ax) * directionY
              }, true);
            }
          } else {
            for (k = 0, i = this.previousCoords.y; directionY == 1 ? i <= coords.y : i >= coords.y; k++, i += directionY) {
              this.mousemove({
                x: this.previousCoords.x + Math.floor((ax * k) / ay) * directionX,
                y: i
              }, true);
            }
          }
        }
      }
      var x = coords.x;
      var y = coords.y;
      var col = integerDivision(x, this.CELL_WIDTH);
      var row = integerDivision(y, this.CELL_HEIGHT);
      var cellX = col * this.CELL_WIDTH;
      var cellY = row * this.CELL_HEIGHT;
      // РЅРµРєРІР°РґСЂР°С‚РЅС‹Р№ cell РЅРµ СѓС‡РёС‚С‹РІР°РµС‚СЃСЏ
      var rx = x - cellX;
      var ry = y - cellY;
      var triangle = rx < this.halfCellWidth ? (
        ry < this.halfCellHeight ? (
          rx > ry ? 0 : 3
        ) : (
          rx + ry > this.CELL_WIDTH ? 2 : 3
        )
      ) : (
        ry < this.halfCellHeight ? (
          rx + ry > this.CELL_WIDTH ? 1 : 0
        ) : (
          rx > ry ? 1 : 2
        )
      );
      this.triangle(
        triangle == 0 || triangle == 2 || triangle == 3 ? cellX : cellX + this.halfCellWidth,
        triangle == 0 || triangle == 3 ? cellY : (
          triangle == 1 ? cellY + this.halfCellHeight : cellY + this.CELL_HEIGHT
        ),
        triangle == 0 || triangle == 1 ? cellX + this.CELL_WIDTH : cellX + this.halfCellWidth,
        triangle == 0 || triangle == 1 ? cellY : cellY + this.halfCellHeight,
        triangle == 3 ? cellX : (
          triangle == 0 ? cellX + this.halfCellWidth : cellX + this.CELL_WIDTH
        ),
        triangle == 0 ? cellY + this.halfCellHeight : cellY + this.CELL_HEIGHT
      );
      if (!mode2) {
        this.busy = false;
        this.previousCoords = coords;
      }
    },
    triangle: function(x0, y0, x1, y1, x2, y2) {
      var pathString = 'M' + x0 + ' ' + y0 + 'L' + x1 + ' ' + y1 + 'L' + x2 + ' ' + y2 + 'L' + x0 + ' ' + y0;
      if (!(pathString in this.triangles))
        this.triangles[pathString] = this.paper.path(pathString);
      if (this.lastTriangle != pathString) {
        var el = this.triangles[pathString];
        el.attr({
          'fill': '#ffffff',
          'stroke-width': 0
        }).animate({
          fill: this.nextColor()
        }, 200);
      }
      this.lastTriangle = pathString;
    },
    nextColor: function() {
      var result = this.colors[this.currentColor];
      if (this.currentColor == this.colors.length - 1)
        this.currentColor = 0;
      else
        this.currentColor++;
      return result;
    }
  }

  Triangles.init();

  $('.main-customers__card').popover({
    placement: 'auto right'
  });

  $('.main-header__call').click(
    function() {
      $('.main-call').toggleClass('main-call_active');
    }
  );

  $('.main-call__close').click(
    function() {
      $('.main-call').toggleClass('main-call_active');
    }
  );

});